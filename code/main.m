
%% Contrast Limited Adaptive Histogram Equalization
clc;
clear all;
close all;
preProcessMethod = 2;  % 1 -> CLAHE
                       % 2 -> Gray level Transformation
                        
candidateExtraction = 1; %1-> Diameter closing       
                         
                
%% Read an input image
[FileName,PathName] = uigetfile('*.*','Select an Image');
inputImage = imread(strcat(PathName,FileName));
%figure,imshow(inputImage);
title('Test Images For Diabetic Retinopathy ');
imgGreen = inputImage;
imgGreen(:,:,1) = 0; 
imgGreen(:,:,3) = 0; 
%figure, imshow(imgGreen),title('Green Intensity Image');
%%  RGB to Gray Image
img = rgb2gray(imgGreen);
%figure, imshow(img), title('RGB to Gray');

if preProcessMethod==1
%% CLAHE
imgOut=adapthisteq(img,'clipLimit',0.05,'Distribution','rayleigh');
%figure,imshow(imgOut),title('CLAHE');

[psnrC, mseC]=psnr(img,imgOut);

else
%% Grey level transformation
imgMean=35;%mean(img(:));
disp(imgMean);
[rmax,cmax]=size(img);
h=imshow(img,[]);
imageModel=imagemodel(h);
displayRange=getDisplayRange(imageModel);
imgMin=displayRange(1);
imgMax=displayRange(2);

imgOut=zeros(size(img));
for i=1:rmax
    for j=1:cmax
        if img(i,j)<=imgMean
            imgOut(i,j)= (.5.* ((255-0)/(imgMean-imgMin).^1).*((img(i,j)-imgMin).^1))+0;
        else
            imgOut(i,j)= (-.5.* ((255-0)/(imgMean-imgMax).^1).*((img(i,j)-imgMax).^1))+255;
        end
    end
end
%figure,imshow(imgOut,[]),title('Grey level transformed image');

[psnrG, mseG]=psnr(img,imgOut);

end

%% Plot PSNR and MSE and Histogram

% psnrValues=[psnrC psnrG];
% mseValues=[mseC mseG];
% 
% figure, subplot(1,2,1);
% bar(psnrValues,.2);
% set(gca,'XTickLabel',{'CLAHE', 'Gray level transf'})
% xlabel('Preprocessing algorithms')
% ylabel('PSNR');
% 
% subplot(1,2,2);
% bar(mseValues,.2);
% set(gca,'XTickLabel',{'CLAHE', 'Gray level transf'})
% xlabel('Preprocessing algorithms')
% ylabel('Mean Square Error');


%% Candidate Extraction

%% Diameter closing


invertImage = InvertIm(imgOut);
%figure, imshow(invertImage), title('Inverted Grayscale image');

binaryImage=im2bw(invertImage,.47); % Convert to equivalent binary image with threshold
%figure, imshow(binaryImage), title('Binary image');
 
invertImage = InvertIm(binaryImage); %Invert the binary image
figure, imshow(invertImage), title('Micro Aneyryms Detection Images');

imgFill=imfill(invertImage,'holes'); % Fill in the holes of a binary image
figure, imshow(imgFill), title(' Boundary Clipping Mask Image ');

% Removing the optic disc

L=bwlabel(imgFill);
s=regionprops(L, 'Area');
area_values = [s.Area];
idx = find((area_values<=1500));
imgODR=ismember(L,idx);
%figure,imshow(imgODR), title('Optic Disc Removed');

diaClose=bwareaopen(imgODR,10,4);% Remove small objects in the image with less than P pixels using 4 or 8 connectivity
%figure, imshow(diaClose), title('Diameter closing');

%% Read an input image
[FileName,PathName] = uigetfile('*.*','Select Boundary Clipping Mask Image');
origImg = imread(strcat(PathName,FileName));
[FileName,PathName] = uigetfile('*.*','Select Micro Aneyryms Detection Images');
distImg = imread(strcat(PathName,FileName));


%If the input image is rgb, convert it to gray image
noOfDim = ndims(origImg);
if(noOfDim == 3)
    origImg = rgb2gray(origImg);
end

noOfDim = ndims(distImg);
if(noOfDim == 3)
    distImg = rgb2gray(distImg);
end

%Size Validation
origSiz = size(origImg);
distSiz = size(distImg);
sizErr = isequal(origSiz, distSiz);
if(sizErr == 0)
    disp('Error: Original Image & Distorted Image should be of same dimensions');
    return;
end

%Mean Square Error 

origImg = double(origImg);
distImg = double(distImg);

[M N] = size(origImg);
error = origImg - distImg;

MSError = sum(sum(error .* error)) / (M * N);
%disp('Mean Square Error = ');
%disp(MSError);

disp(     '    Diabetic Retinopathy '     );
%function AD = AverageDifference(origImg, distImg)

AD = sum(sum(error)) / (M * N);
disp('Average Difference Of Voxel Range  = ');
disp(AD);


%function MD = MaximumDifference(origImg, distImg)

MD = max(max(error));
%disp('Maximum Difference = ');
%disp(MD);


%function NAE = NormalizedAbsoluteError(origImg, distImg)

NAE = sum(sum(abs(error))) / sum(sum(origImg));
disp(' Stage Detection Algoriyhm Value  = ');
disp(NAE);
if((NAE>0.0100)&&(NAE<0.0150))
    disp(' stage1 ');
elseif((NAE>0.0150)&&(NAE<200))
    disp('stage2');
 else
    disp('stage3');
end;  

%function NK = NormalizedCrossCorrelation(origImg, distImg)

NK = sum(sum(origImg .* distImg)) / sum(sum(origImg .* origImg));
%disp('Normalized Cross Correlation = ');
%disp(NK);

%function PSNR = PeakSignaltoNoiseRatio(origImg, distImg)

if(MSError > 0)
    PSNR = 10*log(255*255/MSError) / log(10);
else
    PSNR = 99;
end
disp('PeakSignal to Noiseration = ');
disp(PSNR);
a=PSNR;
disp('After One Year Disease Spread Range : ');
b=2*PSNR/15*1000;
disp(b)
%function SC = StructuralContent(origImg, distImg)

SC = sum(sum(origImg .* origImg)) / sum(sum(distImg .* distImg));

%disp('Structural Content = ');
%disp(SC);









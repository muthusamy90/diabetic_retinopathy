function [ psnr, mse ] = psnr( origImage, processImage )
%PSNR calcuation

[rows columns] = size(origImage);

mseImage = (double(origImage) - double(processImage)) .^ 2;
mse = sum(sum(mseImage)) / (rows * columns);

psnr = 10 * log10( 256^2 / mse);

end

